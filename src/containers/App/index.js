import React from 'react';
import logo from '../../logo.svg';
import Item from '../../components/ListMusic';

import './App.css';

const items = [
    {'id' : '01', 'title' : 'Em Không Sai Chúng Ta Sai', 'artist' : 'ERIK', 'img_url' : 'https://photo-resize-zmp3.zadn.vn/w94_r1x1_jpeg/cover/7/4/0/d/740d5e0fd272d2421d441e9fd5c08fdd.jpg'},
    {'id' : '02', 'title' : 'Cướp Đi Cả Thế Giới', 'artist' : 'Bảo Yến Rosie', 'img_url' : 'https://photo-resize-zmp3.zadn.vn/w94_r1x1_jpeg/cover/d/d/f/c/ddfc0c54f8c99b517539cc11c24f6b88.jpg'},
    {'id' : '03', 'title' : 'Thích Thì Đến', 'artist' : 'Lê Bảo Bình', 'img_url' : 'https://photo-resize-zmp3.zadn.vn/w94_r1x1_jpeg/cover/b/4/4/6/b4461d303cba114b38429c6ea84d9fa2.jpg'},
    {'id' : '04', 'title' : 'Không Thể Cùng Nhau Suốt Kiếp', 'artist' : 'Hòa Minzy', 'img_url' : 'https://photo-resize-zmp3.zadn.vn/w94_r1x1_jpeg/cover/1/7/8/6/17861cfa5213a52daec2c556101fb421.jpg'},
    {'id' : '05', 'title' : 'Tình Anh', 'artist' : 'Đình Dũng', 'img_url' : 'https://photo-resize-zmp3.zadn.vn/w94_r1x1_jpeg/cover/f/6/a/b/f6ab94cda33ac9c190a7ac8cd0315270.jpg'},
];


function App() {
    return (
        <div className="App">
            <div>
                <ul class={'ul-top'}>
                    {items.map(item => (
                        <Item id={item.id} title={item.title} artist={item.artist} img_url={item.img_url}></Item>
                    ))}
                </ul>

            </div>
        </div>
    );
}

export default App;
