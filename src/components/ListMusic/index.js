import React from 'react';
import styles from './module.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCaretRight} from "@fortawesome/free-solid-svg-icons";

export default function (prop) {
    return (
        <li class={'li-top'}>
            <span className={'float-left li-ms li-ms-' + prop.id}>{prop.id}</span>
            <div className={'float-left thumb-img'}>
                <img src={prop.img_url}></img>
                <FontAwesomeIcon className={'icon-play'} icon={faCaretRight} />
            </div>
            <div className={'float-left content-top'}>
                <h4 class={'title'}>{prop.title}</h4>
                <p class={'artist'}>{prop.artist}</p>
            </div>
        </li>
    )
}